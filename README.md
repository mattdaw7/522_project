## To run:
1. Clone the repository
```
git clone git@gitlab.com:mattdaw7/surface_plotting.git
```
2. Build the docker image
```
cd 522_project
cd loss-landscape
sudo docker build -t IMAGE_NAME -f docker/Dockerfile .
```
3. Run an interactive docker container (MAKE SURE YOU REPLACE `/path/to/data` and `/path/to/logs` with the desired local paths to mount as volumes inside your container)
```
sudo docker run -it --rm --name RUN_NAME -v $(pwd):/code IMAGE_NAME
```
4. Inside the docker container, run 
```
cd code
python3 plot_surface.py
```
To see all command-line options, run
```
python3 plot_surface.py --help
```
